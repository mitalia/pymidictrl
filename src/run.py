#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ui.mainwindow import *

instruments = [l.strip() for l in open("../res/midiinstruments.txt", "r").readlines()]

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.instrumentsModel = QStringListModel(instruments)
        self.filterModel = QSortFilterProxyModel()
        self.filterModel.setSourceModel(self.instrumentsModel)
        self.filterModel.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.ui.instrumentsList.setModel(self.filterModel)

    def on_searchInstrumentEdit_textEdited(self, text):
        self.filterModel.setFilterWildcard('*'.join(str(text).split(' ')))

if __name__=='__main__':
    app = QApplication(sys.argv)
    m = MainWindow()
    m.show()
    app.exec_()
